package ru.float_goat.api

import ru.float_goat.dto.{Message, Update, User}

import scala.concurrent.Future

trait TelegramApi {
  def getMe: Future[User]

  def getUpdates(offset: BigInt, timeout: Int = 1, limit: Int = 100): Future[Seq[Update]]

  def send(body: String): Future[Message]

  def setBotToken(token: String): Unit
}
