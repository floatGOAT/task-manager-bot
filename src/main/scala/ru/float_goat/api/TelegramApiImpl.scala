package ru.float_goat.api

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.util.ByteString
import com.typesafe.scalalogging.Logger
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.Json
import ru.float_goat.dto.{Message, Update, User}
import ru.float_goat.exc.TelegramApiException

import scala.concurrent.{ExecutionContextExecutor, Future}

class TelegramApiImpl(apiEndpoint: String) extends TelegramApi {
  private val logger: Logger = Logger[TelegramApiImpl]

  implicit val system: ActorSystem = ActorSystem()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  private var token: String = _

  override def getMe: Future[User] = {
    val uri = buildUri("getMe")
    Http()
      .singleRequest(HttpRequest(uri = uri))
      .flatMap(response => Unmarshal(response).to[Json])
      .map(json => (json \\ "result").head.as[User].toOption)
      .map {
        case Some(user) => user
        case None => throw new TelegramApiException("Can't fetch bot info")
      }
  }

  override def getUpdates(offset: BigInt, timeout: Int, limit: Int): Future[Seq[Update]] = {
    logger.debug("Getting updates...")
    val uri = buildUri("getUpdates").withQuery(buildQuery(offset, timeout, limit))
    logger.debug(s"UpdatesURI = ${uri.toString()}")

    Http()
      .singleRequest(HttpRequest(uri = uri))
      .flatMap(response => Unmarshal(response).to[Json])
      .map(json => (json \\ "result").head.asArray)
      .map {
        case Some(updates) =>
          updates.map(json => json.as[Update].toOption).collect {
            case Some(value) => value
          }
        case None => throw new TelegramApiException("Empty body!")
      }
  }

  override def send(body: String): Future[Message] = {
    val uri = buildUri("sendMessage")
    val requestEntity = HttpEntity(ContentTypes.`application/json`, ByteString(body))
    val request = HttpRequest(method = HttpMethods.POST, uri = uri, entity = requestEntity)

    Http()
      .singleRequest(request)
      .flatMap(response => Unmarshal(response).to[Json])
      .map(json => (json \\ "result").head.as[Message].toOption.get)
  }

  override def setBotToken(token: String): Unit = this.token = token

  /** *************************************************************
   *
   * Private methods
   *
   * ************************************************************* */

  private def buildQuery(offset: BigInt, timeout: Int, limit: Int): Query = {
    Uri.Query.newBuilder
      .addOne("offset" -> offset.toString)
      .addOne("timeout" -> Math.min(timeout, 9999).toString)
      .addOne("limit" -> Math.min(limit, 100).toString)
      .result()
  }

  private def buildUri(route: String): Uri = Uri(s"$apiEndpoint/bot$token/$route")
}
