package ru.float_goat.bot

import ru.float_goat.{ChatID, UserID}

import scala.concurrent.Future

trait TaskManagerBot extends TelegramBot {
  def getTasks(id: ChatID): Future[_]

  def addTask(id: ChatID, description: String): Future[_]

  def acceptTask(id: ChatID, assignee: UserID, number: Int): Future[_]

  def finishTask(id: ChatID, assignee: UserID, number: Int): Future[_]
}
