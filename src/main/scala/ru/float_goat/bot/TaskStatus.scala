package ru.float_goat.bot

import enumeratum.{CirceEnum, Enum, EnumEntry}

sealed trait TaskStatus extends EnumEntry

object TaskStatus extends Enum[TaskStatus] with CirceEnum[TaskStatus] {

  case object Free extends TaskStatus

  case object InProgress extends TaskStatus

  case object Done extends TaskStatus

  override val values: IndexedSeq[TaskStatus] = findValues
}