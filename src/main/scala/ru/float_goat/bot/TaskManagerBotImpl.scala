package ru.float_goat.bot

import akka.actor.ActorSystem
import akka.stream.KillSwitches
import akka.stream.scaladsl.{Flow, Source}
import akka.{Done, NotUsed}
import com.typesafe.scalalogging.Logger
import io.circe.syntax.EncoderOps
import ru.float_goat.api.TelegramApi
import ru.float_goat.db.h2.H2Database
import ru.float_goat.dto._
import ru.float_goat.exc.TaskManagementException
import ru.float_goat.{ChatID, UserID}

import java.io.PrintWriter
import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.{Failure, Success, Try}

class TaskManagerBotImpl(api: TelegramApi) extends TaskManagerBot {
  private val logger: Logger = Logger[TaskManagerBotImpl]

  implicit val system: ActorSystem = ActorSystem()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  private val storedOffsetPath = "src/main/resources/ru/float_goat/bot/offset.txt"

  private val stopPollingKillSwitch = KillSwitches.shared("stopPollingKillSwitch")

  private var database: H2Database = _

  private def singleSource: Source[BigInt, NotUsed] = Source.single(this.offset.get())

  private val offsetSource: Source[BigInt, NotUsed] = Source.cycle(() => Some(this.offset.get()).iterator)

  private val updatesFlow: Flow[Seq[Update], Seq[Message], NotUsed] =
    Flow[Seq[Update]].map(updates => {
      val length = updates.length
      if (length > 0)
        logger.debug(s"Processing $length updates")
      updates.flatMap(upd => {
        val updatedOffset = this.offset.getAndUpdate(prev => prev.max(upd.update_id + 1))
        logger.debug(s"updated offset = $updatedOffset")
        upd.message
      })
    })

  override def getTasks(id: ChatID): Future[Seq[Task]] = database.getChatTasks(id)

  override def addTask(id: ChatID, description: String): Future[Int] = database.getChatTasks(id)
    .map(_.length)
    .map(tasksAlready => Task(id, tasksAlready + 1, description))
    .flatMap(database.addTask)

  override def acceptTask(id: ChatID, assignee: UserID, number: Int): Future[Int] = database.getChatTasks(id)
    .map(tasks => tasks.find(_.number == number))
    .map {
      case Some(task) => task
      case None => throw new TaskManagementException(s"Task #$number doesn't exist.")
    }
    .map { task =>
      if (task.status == TaskStatus.Free)
        task
      else
        throw new TaskManagementException(s"Task #$number already assigned or finished.")
    }
    .flatMap(_ => database.assignTask(id, number, assignee))

  override def finishTask(id: ChatID, assignee: UserID, number: Int): Future[Int] = database.getChatTasks(id)
    .map(tasks => tasks.find(_.number == number))
    .map {
      case Some(task) => task
      case None => throw new TaskManagementException(s"Task #$number doesn't exist.")
    }
    .map { task =>
      if (task.status == TaskStatus.Done)
        throw new TaskManagementException(s"Task #$number already finished.")
      else
        task
    }
    .map { task =>
      if (task.assignee.contains(assignee)) {
        task
      } else {
        throw new TaskManagementException("Task can be finished only by its assignee.")
      }
    }
    .flatMap(_ => database.finishTask(id, number, assignee))

  override def start(): Unit = {
    logger.info("TaskManagerBot start service!")
    loadBotToken("ru/float_goat/bot/token.txt")
      .andThen {
        case Success(token) =>
          logger.info("Bot token set successfully")
          api.setBotToken(token)
      }
      .flatMap(_ => loadOffset())
      .andThen {
        case Success(offset) =>
          this.offset.set(offset)
          logger.info(s"Loaded offset = $offset")
      }
      .flatMap(_ => setUpDatabase())
      .andThen {
        case Success(_) => logger.info("TaskManagerBot database set up successfully")
      }
      .flatMap(_ => api.getMe)
      .andThen {
        case Success(botUser) =>
          logger.debug("Bot identify itself: " + botUser.toString)
          this.id = botUser.id
      }
      .flatMap(_ => singleUpdate())
      .onComplete(runStatus => {
        runStatus match {
          case Failure(exception) =>
            logger.error("TaskManagerBot executes with error: " + exception.getMessage)
          case Success(_) => logger.info("TaskManagerBot executes successfully")
        }
        stop()
      })
  }

  override def stop(): Unit = {
    logger.info("TaskManagerBot stop service.")
    stopPollingKillSwitch.shutdown()
    database.close()
    Some(new PrintWriter(storedOffsetPath)).foreach(p => {
      p.write(this.offset.get().toString())
      p.close()
    })
    system.terminate()
  }

  /** *************************************************************
   *
   * Private methods
   *
   * ************************************************************* */

  private def pollUpdates(): Future[Done] = {
    logger.debug("Start long polling")
    offsetSource
      .throttle(1, 1.second)
      .mapAsync(1)(offset => api.getUpdates(offset))
      .via(updatesFlow)
      .via(stopPollingKillSwitch.flow)
      .runForeach(messages => messages.foreach(handleMessage))
  }

  private def singleUpdate(): Future[_] = {
    logger.trace("Send single updates request")
    singleSource
      .mapAsync(1)(offset => api.getUpdates(offset))
      .via(updatesFlow)
      .via(stopPollingKillSwitch.flow)
      .runForeach(messages => Future.traverse(messages)(msg => handleMessageAsync(msg)))
      .andThen {
        case Success(value) => logger.debug("Success result = " + value)
        case Failure(exception) => logger.error("Fail result = " + exception.getMessage)
      }
      .flatMap(_ => singleUpdate())
  }

  private def handleMessageAsync(message: Message): Future[Message] = {
    processMessage(message)
      .collect {
        case Some(sendMessage) => sendMessage.asJson.toString()
      }
      .andThen {
        case Success(responseJson) => logger.debug("Response = " + responseJson)
      }
      .flatMap(responseJson => api.send(responseJson))
  }

  private def handleMessage(message: Message): Unit = handleMessageAsync(message).onComplete(_ => ())

  private def processMessage(message: Message): Future[Option[SendMessage]] = {
    val chatId = message.chat.id

    message.text
      .filter(_.head == '/')
      .map { text =>
        val firstWord = text.takeWhile(ch => !" @".contains(ch))
        firstWord match {
          case "/list" => this.getTasks(chatId)
            .map(tasks => tasks.sortBy(_.number).map(task => prettyTask(task, message.from)).mkString("\n"))
            .map(text => Some(text))
          case "/add" =>
            val taskDescription = text.dropWhile(_ != ' ').tail
            this.addTask(chatId, taskDescription)
              .transform {
                case Failure(exception) =>
                  logger.error(exception.getMessage, exception)
                  Success(Some(exception.getMessage))
                case Success(_) => Success(None)
              }
          case "/get" =>
            val taskNumber = text.split(" ")(1).toInt
            this.acceptTask(chatId, message.from.get.id, taskNumber)
              .map(_ => message.from
                .map(user => s"${user.first_name} ${user.last_name.getOrElse("")} gets task #$taskNumber")
                .getOrElse(s"Task #$taskNumber was assigned"))
              .transform {
                case Failure(exception) =>
                  logger.error(exception.getMessage, exception)
                  Success(Some(exception.getMessage))
                case Success(text) => Success(Some(text))
              }
          case "/done" =>
            val taskNumber = text.split(" ")(1).toInt
            this.finishTask(chatId, message.from.get.id, taskNumber)
              .transform {
                case Failure(exception) =>
                  logger.error(exception.getMessage, exception)
                  Success(Some(exception.getMessage))
                case Success(_) => Success(None)
              }
          case _ => Future.successful(None)
        }
      }
      .getOrElse(Future.successful(None))
      .map(_.map(SendMessage(chatId, _)))
  }

  private def loadBotToken(tokenFile: String): Future[String] = {
    val `try` = Try {
      val src = scala.io.Source.fromResource(tokenFile)
      val result = src.mkString
      src.close()
      result
    }
    Future.fromTry(`try`)
  }

  private def loadOffset(): Future[BigInt] = {
    val `try` = Try {
      val src = scala.io.Source.fromFile(storedOffsetPath)
      val result = BigInt(src.mkString)
      src.close()
      result
    }
    Future.fromTry(`try`)
  }

  private def setUpDatabase(): Future[Unit] = {
    val `try` = Try {
      this.database = new H2Database()
    }
    Future.fromTry(`try`)
  }

  private def prettyTask(task: Task, assignee: Option[User]): String = {
    val statusPrefix = task.status match {
      case TaskStatus.Free | TaskStatus.InProgress => s"${task.number}."
      case TaskStatus.Done => "+"
    }
    val assigneePrefix = if (task.assignee.isEmpty)
      ""
    else assignee.map { user =>
      user.username
        .map(username => s"@$username")
        .getOrElse(s"${user.first_name} ${user.last_name.getOrElse("")}")
    }
      .getOrElse("")
//    val assigneePrefix = task.assignee
//      .map(userId => s"""<a href="tg://user?id=$userId"></a>""")
//      .getOrElse("")
    s"""$statusPrefix $assigneePrefix "${task.description}""""
  }
}
