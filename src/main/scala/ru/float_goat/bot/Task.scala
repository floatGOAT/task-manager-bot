package ru.float_goat.bot

import ru.float_goat.{ChatID, UserID}

case class Task(chat: ChatID,
                number: Int,
                description: String,
                assignee: Option[UserID] = None,
                status: TaskStatus = TaskStatus.Free)
