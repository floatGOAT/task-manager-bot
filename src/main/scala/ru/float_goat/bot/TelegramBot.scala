package ru.float_goat.bot

import ru.float_goat.UserID

import java.util.concurrent.atomic.AtomicReference

trait TelegramBot {
  /** Telegram UserID of this bot */
  protected var id: UserID = _

  /** Latest processed Update ID */
  protected var offset: AtomicReference[BigInt] = new AtomicReference(BigInt(0))

  def start(): Unit

  def stop(): Unit
}
