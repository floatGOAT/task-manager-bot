package ru.float_goat.exc

class TelegramApiException(message: String) extends RuntimeException(message)
