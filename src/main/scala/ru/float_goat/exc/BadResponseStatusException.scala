package ru.float_goat.exc

class BadResponseStatusException extends TelegramApiException("Response status is not OK")
