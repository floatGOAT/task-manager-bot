package ru.float_goat.exc

class TaskManagementException(message: String) extends RuntimeException(message)
