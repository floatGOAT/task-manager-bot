package ru.float_goat.dto

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class Document(file_id: String,
                    file_unique_id: String,
                    file_name: Option[String],
                    mime_type: Option[String],
                    file_size: Option[Long])

object Document {
  implicit val jsonDecoder: Decoder[Document] = deriveDecoder
  implicit val jsonEncoder: Encoder[Document] = deriveEncoder
}

