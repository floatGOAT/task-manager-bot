package ru.float_goat.dto

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class BotCommand(command: String,
                      description: String)

object BotCommand {
  implicit val jsonDecoder: Decoder[BotCommand] = deriveDecoder
  implicit val jsonEncoder: Encoder[BotCommand] = deriveEncoder
}
