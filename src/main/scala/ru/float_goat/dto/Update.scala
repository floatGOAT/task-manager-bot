package ru.float_goat.dto

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class Update(update_id: Long,
                  message: Option[Message])

object Update {
  implicit val jsonDecoder: Decoder[Update] = deriveDecoder
  implicit val jsonEncoder: Encoder[Update] = deriveEncoder
}
