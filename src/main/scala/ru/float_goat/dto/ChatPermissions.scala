package ru.float_goat.dto

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class ChatPermissions(can_send_messages: Option[Boolean],
                           can_pin_messages: Option[Boolean])

object ChatPermissions {
  implicit val jsonDecoder: Decoder[ChatPermissions] = deriveDecoder
  implicit val jsonEncoder: Encoder[ChatPermissions] = deriveEncoder
}
