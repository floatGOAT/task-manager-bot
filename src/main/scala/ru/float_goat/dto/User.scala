package ru.float_goat.dto

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class User(id: Long,
                is_bot: Boolean,
                first_name: String,
                last_name: Option[String],
                username: Option[String],
                can_join_groups: Option[Boolean])

object User {
  implicit val jsonDecoder: Decoder[User] = deriveDecoder
  implicit val jsonEncoder: Encoder[User] = deriveEncoder
}
