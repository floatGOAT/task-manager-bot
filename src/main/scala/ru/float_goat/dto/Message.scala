package ru.float_goat.dto

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

import java.util.Date

case class Message(message_id: Long,
                   from: Option[User],
                   date: Long,
                   chat: Chat,
                   text: Option[String],
                   document: Option[Document])

object Message {
  implicit val jsonDecoder: Decoder[Message] = deriveDecoder
  implicit val jsonEncoder: Encoder[Message] = deriveEncoder
}
