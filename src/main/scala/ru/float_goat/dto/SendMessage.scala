package ru.float_goat.dto

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class SendMessage(chat_id: Long,
                       text: String,
                       disable_notification: Option[Boolean] = None,
                       reply_to_message_id: Option[Long] = None)

object SendMessage {
  implicit val jsonDecoder: Decoder[SendMessage] = deriveDecoder
  implicit val jsonEncoder: Encoder[SendMessage] = deriveEncoder
}
