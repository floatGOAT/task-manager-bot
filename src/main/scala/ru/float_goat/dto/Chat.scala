package ru.float_goat.dto

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class Chat(id: Long,
                `type`: String,
                pinned_message: Option[Message],
                permissions: Option[ChatPermissions])

object Chat {
  implicit val jsonDecoder: Decoder[Chat] = deriveDecoder
  implicit val jsonEncoder: Encoder[Chat] = deriveEncoder
}


