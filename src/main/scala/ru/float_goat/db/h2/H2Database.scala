package ru.float_goat.db.h2

import ru.float_goat.bot.{Task, TaskStatus}
import ru.float_goat.{ChatID, UserID}
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcBackend.Database

import scala.concurrent.Future

class H2Database() {
  private val database = Database.forConfig("h2mem1")
  database.run(TaskTableConnector.AllTasks.schema.create)

  def getChatTasks(id: ChatID): Future[Seq[Task]] = database.run(TaskTableConnector.getChatTasks(id))

  def addTask(newTask: Task): Future[Int] = database.run(TaskTableConnector.addTask(newTask))

  def assignTask(id: ChatID, number: Int, assignee: UserID): Future[Int] =
    database.run(TaskTableConnector.updateTask(id, number, assignee, TaskStatus.InProgress))

  def finishTask(id: ChatID, number: Int, assignee: UserID): Future[Int] =
    database.run(TaskTableConnector.updateTask(id, number, assignee, TaskStatus.Done))

  def close(): Unit = database.close()
}
