package ru.float_goat.db.h2

import ru.float_goat.bot.TaskStatus._
import ru.float_goat.bot.{Task, TaskStatus}
import ru.float_goat.{ChatID, UserID}
import slick.ast.BaseTypedType
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcType
import slick.lifted.{PrimaryKey, ProvenShape}

class TaskTable(tag: Tag) extends Table[Task](tag, "TASKS") {
  import TaskTable.statusColumnType

  def compositeKey: PrimaryKey = primaryKey("PK", chatId->number)

  def chatId: Rep[ChatID] = column("CHAT_ID")
  def number: Rep[Int] = column("NUMBER")
  def description: Rep[String] = column("DESCRIPTION")
  def assignee: Rep[Option[UserID]] = column("ASSIGNEE", O.Default(None))
  def status: Rep[TaskStatus] = column("STATUS", O.Default(Free))

  override def * :ProvenShape[Task] = (chatId, number, description, assignee, status).mapTo[Task]
}

object TaskTable {
  /**
   * Provide TaskStatus <=> Int conversion which allows store TaskStatus in DB table
   */
  implicit val statusColumnType: JdbcType[TaskStatus] with BaseTypedType[TaskStatus] =
    MappedColumnType.base[TaskStatus, Int](
      {
        case Free => 0
        case InProgress => 1
        case Done => 2
      }, // map TaskStatus to Int
      {
        case 0 => Free
        case 1 => InProgress
        case 2 => Done
        case _ => throw new RuntimeException("Int value out of TaskStatus values range")
      } // map Int to TaskStatus
    )
}