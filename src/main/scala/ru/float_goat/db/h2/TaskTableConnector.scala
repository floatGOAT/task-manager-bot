package ru.float_goat.db.h2

import ru.float_goat.{ChatID, UserID}
import ru.float_goat.bot.{Task, TaskStatus}
import slick.dbio.Effect
import slick.jdbc.H2Profile.api._
import slick.sql.FixedSqlAction


object TaskTableConnector {
  import TaskTable.statusColumnType

  val AllTasks = TableQuery[TaskTable]

  def getChatTasks(id: ChatID): FixedSqlAction[Seq[Task], NoStream, Effect.Read] = AllTasks.filter(_.chatId === id).result

  def addTask(newTask: Task): FixedSqlAction[Int, NoStream, Effect.Write] = AllTasks += newTask

  def updateTask(id: ChatID, number: Int, assignee: UserID, newStatus: TaskStatus): FixedSqlAction[Int, NoStream, Effect.Write] = {
    AllTasks
      .filter(_.chatId === id)
      .filter(_.number === number)
      .map(task => task.assignee -> task.status)
      .update(Some(assignee) -> newStatus)
  }
}