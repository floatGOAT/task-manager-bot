package ru.float_goat

import ru.float_goat.api.{TelegramApi, TelegramApiImpl}
import ru.float_goat.bot.{TaskManagerBot, TaskManagerBotImpl}

import scala.io.StdIn

object Main extends App {
  val api: TelegramApi = new TelegramApiImpl("https://api.telegram.org")
  val bot: TaskManagerBot = new TaskManagerBotImpl(api)
  bot.start()
  println("Press ENTER to stop the bot")
  StdIn.readLine()
  bot.stop()
  sys.exit(0)
}
