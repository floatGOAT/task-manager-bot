package ru.float_goat.db.h2

import org.scalactic.source
import org.scalatest.compatible
import org.scalatest.funsuite.AsyncFunSuite
import ru.float_goat.bot.{Task, TaskStatus}
import slick.dbio.{DBIOAction, Effect, NoStream}
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.H2Profile.api._

import ru.float_goat.db.h2.TaskTableConnector.AllTasks

import java.util.UUID

abstract class TasksDatabaseSuite extends AsyncFunSuite {
  protected def test[R, S <: NoStream, E <: Effect](testName: String)
                                                   (testFun: DBIOAction[compatible.Assertion, S, E])
                                                   (implicit pos: source.Position): Unit = {
    super.test(testName) {

      val db = Database.forURL(
        s"jdbc:h2:mem:${UUID.randomUUID()}",
        driver = "org.h2.Driver",
        keepAliveConnection = true
      )

      db.run(initSchema
        .andThen(AllTasks ++= SampleTasks)
      )
        .flatMap(_ => db.run(testFun))
        .andThen { case _ => db.close() }
    }
  }

  private val initSchema = TaskTableConnector.AllTasks.schema.create

  protected val SampleTasks = Seq(
    Task(1L, 1, "First task in this chat!", Some(11L), TaskStatus.InProgress),
    Task(2L, 1, "This task isn't assigned", None, TaskStatus.Free)
  )
}
