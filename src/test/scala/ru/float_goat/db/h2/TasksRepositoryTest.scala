package ru.float_goat.db.h2

import org.scalatest.matchers.should.Matchers
import ru.float_goat.bot.{Task, TaskStatus}
import slick.jdbc.H2Profile.api._
import ru.float_goat.db.h2.TaskTableConnector._

class TasksRepositoryTest extends TasksDatabaseSuite with Matchers {

  test("AllTasks.result should query all tasks") {
    for {
      orders <- AllTasks.result
    } yield orders should contain theSameElementsAs SampleTasks
  }

  test("getChatTasks should return tasks in chat") {
    for {
      task <- getChatTasks(1L)
    } yield assert(task.contains(SampleTasks.head))
  }

  test("getChatTasks should return None if chat not found") {
    for {
      none <- getChatTasks(-1L)
    } yield assert(none.isEmpty)
  }

  test("addTask should add task to database") {
    val newTask = Task(1L, 2, "Test tasks insertion into database.")
    val a = for {
      _ <- addTask(newTask)
      tasks <- getChatTasks(1L)
    } yield assert(tasks.contains(newTask))
    a
  }

  test("updateTaskStatus should update task status") {
    val newStatus = TaskStatus.InProgress
    val assigneeId = 22L
    for {
      _ <- updateTask(2L, 1, assigneeId, newStatus)
      tasks <- getChatTasks(2L)
    } yield {
      assert(tasks.head.status == newStatus)
      assert(tasks.head.assignee.contains(assigneeId))
    }
  }
}


