name := "TaskManagerBot"

version := "1.0"

scalaVersion := "2.13.5"

val circeVersion = "0.13.0"
val slickVersion = "3.3.3"
val AkkaVersion = "2.6.4"
val AkkaHttpVersion = "10.2.4"
val ScalaLoggingVersion = "3.9.5"

libraryDependencies ++= Seq(

  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion,

  "com.beachape" %% "enumeratum" % "1.6.1",
  "com.beachape" %% "enumeratum-circe" % "1.6.1",

  "com.typesafe.slick" %% "slick" % slickVersion,
  "com.typesafe.slick" %% "slick-hikaricp" % slickVersion,
  "com.h2database" % "h2" % "1.4.200",

  "com.typesafe.akka" %% "akka-actor-typed" % AkkaVersion,
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
  "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-core" % AkkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-testkit" % AkkaHttpVersion % Test,

  "de.heikoseeberger" %% "akka-http-circe" % "1.36.0",

  "com.typesafe.scala-logging" %% "scala-logging" % ScalaLoggingVersion,
  "ch.qos.logback" % "logback-classic" % "1.4.6",

  "org.scalatest" %% "scalatest" % "3.2.2" % Test,
)
