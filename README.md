# TaskManagerBot

![Version](https://gitlab.com/floatGOAT/task-manager-bot/badges/main/release.svg)

This is my course project for [Tinkoff Scala course](https://fintech.tinkoff.ru/study/fintech/scala/).
This is a Telegram Bot written in Scala.

## Purpose
This Bot's aim is to simplify task management in group chats. Imagine yourself planning the event: birthday party,
outdoor trip or even agile sprint - with the group of mates. You have a bunch of tasks which should be written,
assigned to someone, and eventually completed. The TaskManagerBot can help!  

## Commands
4 commands is enough to successfully interact with TaskManagerBot:
- `/add <task description>` - adds new task to list
- `/list` - shows the list of current tasks
- `/get <task ID>` - assigns the task to yourself
- `/done <task ID>` - marks the previously got task as done

## Deployment
Unfortunately, your cannot run this Bot by yourself because it requires private Telegram API token.
